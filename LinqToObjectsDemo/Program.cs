﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LinqToObjectsDemo
{
    class Program
    {
        static void Main()
        {
            List<int> ilist = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            List<int> ilist2 = Enumerable.Range(7, 5).ToList();

            var q1 = ilist.Where(i => i % 2 == 0)
                .Select(i => new { Number = i, Square = i * i });

            //var q1 = from i in ilist
            //    where i%2 == 0
            //    select new {Number = i, Square = i*i};
            //q1.Dump();

            //ilist.Add(10);

            //q1.Dump();

            string[] szavak = { "éa", "gb", "ec" };

            Console.WriteLine(Thread.CurrentThread.CurrentCulture.Name);

            var s = Thread.CurrentThread.CurrentCulture.CompareInfo.Compare("cz", "cs");
            //Console.WriteLine(s);

            var q2 = from sz in szavak
                     orderby sz[0] descending, sz[1]
                     //orderby sz[1]
                     select sz;
            var q2b = szavak
                .OrderByDescending(sz => sz[0], new MagyarComparer())
                .ThenBy(sz => sz[1], new MagyarComparer());
            //q2.Dump();

            ilist.Add(12);
            var q3 = from i in ilist
                     group i by i % 2 == 0
                into g
                     orderby g.Count()
                     where g.Count() > 4
                     select new { Key = g.Key, Count = g.Count() };
            //q3.Dump();
            //foreach (IGrouping<bool, int> group in q3)
            //{
            //    Console.WriteLine(group.Key);
            //    foreach (int i in group)
            //    {
            //        Console.WriteLine("\t" + i);
            //    }
            //}

            List<int> squares = new List<int> { 9, 16, 64, 144, 169 };
            var q4 = from i in ilist
                     join j in squares
                         on i * i equals j
                     select i;
            //q4.Dump();

            var q5 = from i in ilist
                     group i by i % 2 == 0
                into g
                     let sum = g.Sum()
                     orderby sum
                     select new { Key = g.Key, Count = g.Count(), Sum = sum };
            //q5.Dump();

            var q6 = (from n in (from i in ilist
                                join j in squares
                                    on i * i equals j
                                select i)
                     where n % 2 == 0
                     select n).First();
            //q6.Dump();
            //Console.WriteLine(q6);

            //Particionalas();

            bool any = ilist.Any(i => i*i == 64);
            bool all = ilist.All(i => i < 12);

            int x = (from y in ilist
                where y > 10
                select y).SingleOrDefault();

            var sum2 = ilist.Sum();
            var min = ilist.Min();
            var max = ilist.Max();
            var avg = ilist.Average();

            List<object> ok = new List<object> { 1,2,3, "alma", "béka"};
            var intek = ok.OfType<int>();

            Console.ReadLine();
        }

        private static void Particionalas()
        {
            var list = Enumerable.Range(0, 95).ToList();
            for (int i = 0; i < list.Count(); i += 10)
            {
                var group = list.Skip(i).Take(10);
                foreach (int n in group)
                {
                    Console.Write(n + ", ");
                }
                Console.ReadLine();
            }
        }
    }

    static class EnumerableExtender
    {
        public static void Dump<T>(this IEnumerable<T> list)
        {
            foreach (T item in list) Console.WriteLine(item);
        }
    }

    class MagyarComparer : IComparer<char>
    {
        public int Compare(char x, char y)
        {
            return new CultureInfo("hu-HU").CompareInfo.Compare(
                x.ToString(), y.ToString());
        }
    }
}