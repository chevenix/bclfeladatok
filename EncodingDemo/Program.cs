﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace EncodingDemo
{
    class Program
    {
        private const string FILENAME = "test.txt";
        private const string SZOVEG = "árvíztűrő tükörfúrógép";
        static void Main()
        {
            DisplayEncodings();

            Console.Write("Add meg a kódlap számát: ");
            int codepage = int.Parse(Console.ReadLine());
            Encoding e = Encoding.GetEncoding(codepage);

            if (File.Exists(FILENAME))
            {
                Console.WriteLine(FILENAME + " beolvasása.");
                using (StreamReader sr = new StreamReader(FILENAME, e))
                {
                    Console.WriteLine(sr.ReadToEnd());
                }
            }
            else
            {
                Console.WriteLine(FILENAME + " létehozása.");
                using (StreamWriter sw = new StreamWriter(FILENAME, false, e))
                {
                    sw.WriteLine(SZOVEG);
                }
            }

            Console.ReadLine();
        }

        private static void DisplayEncodings()
        {
            foreach (EncodingInfo info in Encoding.GetEncodings())
            {
                Console.WriteLine("Név: {0},\tkódlap: {1}", 
                    info.Name, info.CodePage);
            }
        }
    }
}