﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace XmlSerializationDemo
{
    public class Person
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private DateTime _birthDate;
        public DateTime BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; }
        }
        
        private int _age;
        [XmlIgnore]
        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        public Person(string name, DateTime birthDate, int age)
        {
            _name = name;
            _birthDate = birthDate;
            _age = age;
        }

        public Person()
        {
            
        }
    }
}