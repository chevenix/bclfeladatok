﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SemaphoreDemo
{
    class Program
    {
        private static Semaphore semaphore = new Semaphore(2, 2);

        static void Main()
        {
            Thread[] ts = new Thread[10];
            for (int i = 0; i < ts.Length; i++)
            {
                ts[i] = new Thread(DoWork);
                ts[i].Start();
            }
            foreach (var t in ts) t.Join();

            Console.WriteLine("Main vége");
            Console.ReadLine();
        }

        static void DoWork()
        {
            Console.WriteLine("ELINDULT: " + Thread.CurrentThread.ManagedThreadId);
            semaphore.WaitOne();
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
                Thread.Sleep(500);
            }
            semaphore.Release(1);
            Console.WriteLine("LEÁLLT: " + Thread.CurrentThread.ManagedThreadId);
        }
    }
}