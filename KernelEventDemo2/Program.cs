﻿using System;
using System.Threading;

namespace KernelEventDemo2
{
    class Program
    {
        static void Main()
        {
            EventWaitHandle myevent;
            try
            {
                myevent = EventWaitHandle.OpenExisting("MYKERNELEVENT");
                Console.WriteLine("Várakozás az eseményre...");
                myevent.WaitOne();
                myevent.Reset();
                Console.WriteLine("Futtatom a kódomat...");
                Thread.Sleep(5000);
                Console.WriteLine("Kód vége...");
                myevent.Set();
                Console.ReadLine();
            }
            catch (WaitHandleCannotBeOpenedException)
            {
                Console.WriteLine("Nincs esemény...");
                Thread.Sleep(3000);
                return;
            }
        }
    }
}