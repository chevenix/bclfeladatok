﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace APMDEmo
{
    class Program
    {
        static void Main()
        {
            //int length = GetLength("alma");

            Func<string, int> f = new Func<string, int>(GetLength);
            //var iar = f.BeginInvoke("alma", null, null);
            //while (iar.IsCompleted != true)
            //{
            //    Console.Write(".");
            //    Thread.Sleep(300);
            //}
            //int length = f.EndInvoke(iar);
            //Console.WriteLine(length);

            f.BeginInvoke("alma", GetLengthCompleted, f);

            Console.WriteLine("Tovább futott a main.");

            Console.ReadLine();
        }

        private static void GetLengthCompleted(IAsyncResult ar)
        {
            Func<string, int> f = ar.AsyncState as Func<string, int>;
            int length = f.EndInvoke(ar);
            Console.WriteLine(length);
        }

        private static int GetLength(string s)
        {
            Thread.Sleep(4000);
            return s.Length;
        }

        //private static IAsyncResult BeginGetLength(string s, AsyncCallback callback, object state)
        //{
        //}

        //private static int EndGetLength(IAsyncResult iar)
        //{
        //}
    }
}