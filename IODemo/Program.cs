﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace IODemo
{
    class Program
    {
        static void Main()
        {
            //Fájlok();
            //Meghajtók();
            //PathTest();
            //FileSystemWatcherTest();
            Encodings();

            Console.ReadLine();
        }

        private static void Encodings()
        {
            string s = "árvíztűrő tükörfúrógép";
            byte[] bs = Encoding.Unicode.GetBytes(s);
            if (!File.Exists("encoding.txt"))
            {
                using (FileStream fs = new FileStream("encoding.txt", FileMode.Create))
                {
                    fs.Write(bs, 0, bs.Length);
                } 
            }
            else
            {
                using (FileStream fs = File.Open("encoding.txt", FileMode.Open))
                {
                    byte[] bs2 = new byte[fs.Length];
                    fs.Read(bs2, 0, bs2.Length);
                    string s2 = Encoding.Unicode.GetString(bs2);
                    Console.WriteLine(s2);
                }
            }
        }

        private static void FileSystemWatcherTest()
        {
            FileSystemWatcher fsw = new FileSystemWatcher(
                @"C:\temp", "*.txt");
            fsw.Created += Fsw_Created;
            fsw.EnableRaisingEvents = true;
        }

        private static void Fsw_Created(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine("Létrejött: " + e.Name);
        }

        private static void PathTest()
        {
            string path1 = @"C:\temp\szoveg.txt";
            if (File.Exists(path1))
            {
                string path2 = Path.ChangeExtension(path1, ".cpy");
                File.Copy(path1, path2);
                Console.WriteLine("másolva");
            }


        }

        private static void Meghajtók()
        {
            foreach (DriveInfo di in DriveInfo.GetDrives())
            {
                Console.WriteLine(di.Name);
                Console.WriteLine(di.DriveType);
                if (di.IsReady)
                {
                    Console.WriteLine(di.VolumeLabel);
                    foreach (DirectoryInfo info in di.RootDirectory.GetDirectories())
                    {
                        Console.WriteLine("\t[" + info.Name + "]");
                    }
                    foreach (FileInfo info in di.RootDirectory.GetFiles())
                    {
                        Console.WriteLine("\t" + info.Name);
                    }
                }
            }
        }

        private static void Fájlok()
        {
            DirectoryInfo temp = new DirectoryInfo(@"C:\temp");
            if (temp.Exists)
            {
                //Console.WriteLine("Má van ilyen.");
                //temp.Delete(true);

                //FileStream fs = new FileStream(@"C:\temp\szoveg.txt", FileMode.Open);
                //fs.WriteByte(66);
                //fs.WriteByte(67);
                //fs.WriteByte(68);
                //fs.WriteByte(69);
                //fs.WriteByte(42);
                //fs.Flush();
                //fs.Close();
                //fs.Dispose();

                //using (FileStream fs = new FileStream(@"C:\temp\szoveg.txt", FileMode.Open))
                //{
                //    fs.WriteByte(66);
                //    fs.WriteByte(67);
                //    fs.WriteByte(68);
                //    fs.WriteByte(69);
                //    fs.WriteByte(42);
                //}

                StreamReader sr = new StreamReader(@"C:\temp\szoveg.txt");
                //Console.WriteLine(sr.ReadToEnd());

                while (!sr.EndOfStream)
                {
                    Console.WriteLine(sr.ReadLine());
                }

                sr.Dispose();



                Console.WriteLine(Environment.NewLine);

                //StreamWriter sw = new StreamWriter(@"C:\temp\szoveg.txt", true);
                StreamWriter sw = new StreamWriter(new FileStream(@"C:\temp\szoveg.txt", FileMode.Append));
                sw.WriteLine(DateTime.Now.ToLongTimeString());
                sw.Close();
            }
            else
            {
                temp.Create();
                Console.WriteLine("Létrehozva: " + temp.FullName
                                  + " " + temp.LastWriteTimeUtc);

                FileInfo szoveg = new FileInfo(@"C:\temp\szoveg.txt");
                if (!szoveg.Exists)
                {
                    szoveg.Create();
                    Console.WriteLine("Fájl létrehozva");
                }
            }
        }
    }
}