﻿using System;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace BCLFeladat
{
    class Program
    {
        //private const string FILENAME = "celebs.txt";
        private const string FILENAMEXML = "celebs.xml";
        private const string FILENAMEBIN = "celebs.txt";
        static void Main()
        {
            DisplayEncodings();
            Console.Write("Kódtábla száma: ");
            Encoding e = Encoding.GetEncoding(int.Parse(Console.ReadLine()));


            BinaryFormatter bf = new BinaryFormatter();
            XmlSerializer xs = new XmlSerializer(typeof(Celeb[]));
            if (File.Exists(FILENAMEBIN))
            {
                //StreamReader sr = new StreamReader(FILENAME);
                //Console.WriteLine(sr.ReadToEnd());
                //sr.Close();

                //Console.WriteLine(File.ReadAllText(FILENAME));
                //foreach (string s in File.ReadLines(FILENAME))
                //    Console.WriteLine(s);

                
                //string[] sorok = File.ReadLines(FILENAME, e).ToArray();
                //Celeb[] celebek = new Celeb[int.Parse(sorok[0])];
                //for (int i = 1; i < sorok.Length; i++)
                //{
                //    Celeb c = new Celeb();
                //    c.Name = sorok[i].Split(new string[] {"|"}, StringSplitOptions.None)[0];
                //    c.IQ = int.Parse(sorok[i].Split(new string[] {"|"}, StringSplitOptions.None)[1]);
                //    celebek[i-1] = c;
                //}
                //foreach (Celeb c in celebek)
                //{
                //    Console.WriteLine(c.Name + " " + c.IQ);
                //}

                using (FileStream fs = File.Open(FILENAMEBIN, FileMode.Open))
                {
                    Celeb[] celebek = bf.Deserialize(fs) as Celeb[];
                    foreach (Celeb c in celebek)
                        Console.WriteLine(c.Name + " " + c.IQ);
                }

                Console.ReadLine();
            }
            else if (File.Exists(FILENAMEXML))
            {
                using (FileStream fs = File.Open(FILENAMEXML, FileMode.Open))
                {
                    Celeb[] celebek = xs.Deserialize(fs) as Celeb[];
                    foreach (Celeb c in celebek)
                        Console.WriteLine(c.Name + " " + c.IQ);
                }

                Console.ReadLine();
            }
            else
            {
                Console.Write("[B]ináris, vagy [X]ML? ");
                string mode = Console.ReadLine();

                Celeb[] celebek = new Celeb[] {new Celeb(), new Celeb(), new Celeb() };
                for (int i = 0; i < celebek.Length; i++)
                {
                    Console.Write("Név: ");
                    string input = Console.ReadLine();
                    //Kovács Kristóf, Kovács-Kovács Kristóf, 
                    //Kovács-Kovács Kristóf Kristóf
                    string regex = @"^([A-ZÁÉÍÓÖÚÜŰ][a-záéíóöúüű]+)(-[A-ZÁÉÍÓÖÚÜŰ][a-záéíóöúüű]+)? [A-ZÁÉÍÓÖÚÜŰ][a-záéíóöúüű]+$";
                    if (Regex.IsMatch(input, regex))
                    {
                        celebek[i].Name = input;
                    }
                    else
                    {
                        celebek[i].Name = "Celeb Cecília";
                    }
                }
                
                //StreamWriter sw = new StreamWriter(FILENAME, false, e);
                //sw.WriteLine(celebek.Length);
                //foreach (Celeb c in celebek)
                //{
                //    sw.WriteLine(c.Name + "|" + c.IQ);
                //}
                //sw.Close();

                using (FileStream fs = File.Create(mode == "B" ? FILENAMEBIN : FILENAMEXML))
                {
                    if (mode == "B")
                    {
                        bf.Serialize(fs, celebek);
                        Console.WriteLine("Binárisan sorosítva.");
                    }
                    else
                    {
                        xs.Serialize(fs, celebek);
                        Console.WriteLine("XML-ben sorosítva.");
                    }
                    Console.ReadLine();
                }
            }
        }

        private static void DisplayEncodings()
        {
            foreach (EncodingInfo info in Encoding.GetEncodings())
            {
                Console.WriteLine("Név: {0},\tkódlap: {1}",
                    info.Name, info.CodePage);
            }
        }
    }
}