﻿using System;

namespace BCLFeladat
{
    [Serializable]
    public class Celeb
    {
        //private string _name;
        //public string Name
        //{
        //    get { return _name; }
        //    set { _name = value; }
        //}

        public string Name { get; set; }

        private int _iq = new Random().Next(0, 100);
        public int IQ
        {
            get { return _iq; }
            set { _iq = value; }
        }

    }
}