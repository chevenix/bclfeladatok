﻿using System;
using System.Threading;

namespace ThreadingDemo
{
    class Counter
    {
        private static readonly object _locker = new object();

        private static int _count;
        public static int Count
        {
            get { return _count; }
        }

        public static void Increment()
        {
            for (int i = 0; i < 1000000; i++)
            {
                //lock (_locker)
                //{
                //    _count++; 
                //}
                Interlocked.Increment(ref _count);
            }
        }
    }
}