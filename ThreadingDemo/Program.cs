﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ThreadingDemo
{
    class Program
    {
        static void Main()
        {
            //Alapok();
            //CounterDemo();
            Monitor();

            Console.ReadLine();
        }

        private static void Monitor()
        {
            Console.WriteLine("MAIN: " + Thread.CurrentThread.ManagedThreadId);
            Thread t1 = new Thread(Worker.DoWorkLockingConsole);
            Thread t2 = new Thread(Worker.DoWorkLockingConsole);
            
            lock (typeof(Console))
            {
                t1.Start(ConsoleColor.Cyan);
                t2.Start(ConsoleColor.Yellow);

                Console.ForegroundColor = ConsoleColor.Red;

                t1.Join();
                t2.Join();
            }

            Console.WriteLine("Mindnek vége!");
        }

        private static void CounterDemo()
        {
            Thread[] ts = new Thread[10];
            for (int i = 0; i < ts.Length; i++)
            {
                ts[i] = new Thread(Counter.Increment);
                ts[i].Start();
            }
            foreach (var t in ts) t.Join();
            Console.WriteLine(Counter.Count);
        }

        private static void Alapok()
        {
            Console.WriteLine("MAIN: " + Thread.CurrentThread.ManagedThreadId);
            Thread t1 = new Thread(Worker.DoWork);
            Thread t2 = new Thread(Worker.DoWork);

            lock (typeof (Console))
            {
                t1.Start(ConsoleColor.Cyan);
                t2.Start(ConsoleColor.Yellow);

                Console.ForegroundColor = ConsoleColor.Red;

                t1.Join();
                t2.Join();
            }

            Console.WriteLine("Mindnek vége!");
        }
    }

    class Worker
    {
        private static readonly object _locker = new object();

        public static void DoWork(object o)
        {
            ConsoleColor cc = (ConsoleColor) o;
            for (int i = 0; i < 10; i++)
            {
                lock (_locker)
                {
                    Console.ForegroundColor = cc;
                    Console.WriteLine(Thread.CurrentThread.ManagedThreadId); 
                }
                Thread.Sleep(200);
            }
        }

        public static void DoWorkLockingConsole(object o)
        {
            ConsoleColor cc = (ConsoleColor)o;
            for (int i = 0; i < 10; i++)
            {
                if (Monitor.TryEnter(typeof(Console), 300))
                {
                    try
                    {
                        Console.ForegroundColor = cc;
                        Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
                    }
                    finally
                    {
                        Monitor.Exit(typeof(Console));
                    }
                }
                Thread.Sleep(200);
            }
        }
    }
}