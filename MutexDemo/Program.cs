﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MutexDemo
{
    class Program
    {
        private const string MUTEXNAME = "MYMUTEX";
        static void Main()
        {
            Mutex m = null;
            try
            {
                m = Mutex.OpenExisting(MUTEXNAME);
                Console.WriteLine("ez a program már fut...");
                Thread.Sleep(3000);
                return;
            }
            catch (WaitHandleCannotBeOpenedException)
            {
                m = new Mutex(true, MUTEXNAME);
                Console.WriteLine("Mutex létrehozva...");
            }



            Console.ReadLine();
        }
    }
}