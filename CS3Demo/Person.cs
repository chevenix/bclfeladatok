﻿using System;

namespace CS3Demo
{
    class Person
    {
        public int Age { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
    }

    static class Int32Extender
    {
        public static bool IsValidAge(this int age)
        {
            return (age > 0 && age < 130);
        }
    }
}