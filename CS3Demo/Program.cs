﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CS3Demo
{
    class Program
    {
        static void Main()
        {
            //Initializers();
            //AnonymousTypes();
            //Lambda();

            Person p = new Person() { Age = 8, Name = "Stan"};
            bool validAge = p.Age.IsValidAge();
            int i = 8;
            validAge = i.IsValidAge();

            Console.ReadLine();
        }

        private static void Lambda()
        {
            Func<int, int, string> f1 = delegate(int i, int j)
            {
                return (i + j).ToString();
            };
            string s1 = f1(1, 2);
            Func<int, int, string> f2 = (i, j) => (i + j).ToString();
            string s2 = f2(1, 2);

            var ppl = new List<Person>
            {
                new Person {Name = "Stan", Age = 8, City = "South Park"},
                new Person() {Name = "Kyle", Age = 8, City = "South Park"},
                new Person() {Name = "Eric", Age = 7, City = "South Park"}
            };

            var emberek = ppl.Select(p => new {p.Name, p.Age}).Where(
                x => x.Age > 12);
        }

        private static void AnonymousTypes()
        {
            var ppl = new List<Person>
            {
                new Person {Name = "Stan", Age = 8, City = "South Park"},
                new Person() {Name = "Kyle", Age = 8, City = "South Park"},
                new Person() {Name = "Eric", Age = 7, City = "South Park"}
            };

            //List<Person> spk = new List<Person>();
            //foreach (Person p in ppl)
            //{
            //    if (p.City == "South Park")
            //    {
            //        spk.Add(p);
            //    }
            //}

            var o = new { ppl[0].Name, Kor = ppl[0].Age };
            //Console.WriteLine(o.ToString());
            Console.WriteLine(o.Name + " " + o.Kor);

            var i = 8;
            //i = "";
            var s = "";
            //s = 33;
            //var x = null;
        }

        private static void Initializers()
        {
            int[] intek = {1, 2, 3};

            List<Person> ppl = new List<Person>
            {
                new Person {Name = "Stan", Age = 8, City = "South Park"},
                new Person() {Name = "Kyle", Age = 8, City = "South Park"},
                new Person() {Name = "Eric", Age = 7, City = "South Park"}
            };

            Dictionary<string, int> dict = new Dictionary<string, int>()
            {
                {"alma", 0},
                {"béka", 10},
                {"cékla", -30},
            };
        }
    }
}