﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace BinarySerializationDemo
{
    [Serializable]
    class Person :IDeserializationCallback
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private DateTime _birthDate;
        public DateTime BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; }
        }

        [NonSerialized]
        private int _age;
        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        public Person(string name, DateTime birthDate, int age)
        {
            _name = name;
            _birthDate = birthDate;
            _age = age;
        }

        public void OnDeserialization(object sender)
        {
            _age = (int)((DateTime.Now - _birthDate).TotalDays/365.25);
        }
    }
}