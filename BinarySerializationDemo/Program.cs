﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace BinarySerializationDemo
{
    class Program
    {
        private const string FILENAME = "person.txt";
        static void Main()
        {
            BinaryFormatter bf = new BinaryFormatter();
            if (File.Exists(FILENAME))
            {
                using (FileStream fs = File.Open(FILENAME, FileMode.Open))
                {
                    Person p = bf.Deserialize(fs) as Person;
                    Console.WriteLine("Név: {0}, kor: {1}, született: {2}",
                        p.Name, p.Age, p.BirthDate);
                }
            }
            else
            {
                Person p = new Person("Stan",
                    DateTime.Now.Subtract(new TimeSpan(1000, 0, 0, 0)), 2);
                using (FileStream fs = new FileStream(FILENAME, FileMode.Create))
                {
                    bf.Serialize(fs, p);
                }
            }

            Console.ReadLine();
        }
    }
}