﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LinqToXMLDemo
{
    class Program
    {
        static void Main()
        {
            //XElement root = new XElement("products",
            //    new XElement("product",
            //        new XAttribute("id", 0),
            //        new XElement("name", "bor"),
            //        new XElement("price", 500)),
            //    new XElement("product",
            //        new XAttribute("id", 1),
            //        new XElement("name", "sör"),
            //        new XElement("price", 200)),
            //    new XElement("product",
            //        new XAttribute("id", 2),
            //        new XElement("name", "pálinka"),
            //        new XElement("price", 1500)));

            //Console.WriteLine(root);
            //XDocument xdoc = new XDocument(root);
            //xdoc.Save("products.xml");

            XDocument doc = XDocument.Load("products.xml");
            //Console.WriteLine(doc);

            var items = from e in doc.Descendants("product")
                where (int) e.Element("price") > 400
                orderby (string) e.Element("name") descending 
                select new
                {
                    Id = (int)e.Attribute("id"),
                    Name = (string)e.Element("name")
                };
            foreach (var item in items)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }
    }
}