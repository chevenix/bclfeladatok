﻿using System;
using System.Threading;

namespace KernelEventDemo
{
    class Program
    {
        static void Main()
        {
            EventWaitHandle myevent = new EventWaitHandle(false, 
                EventResetMode.ManualReset, "MYKERNELEVENT");
            Console.WriteLine("Event létrehozva, üss Enter a továbblépéshez!");
            Console.ReadLine();

            Console.WriteLine("Event beállítva.");
            myevent.Set();

            Console.ReadLine();
        }
    }
}