﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RegExDemo
{
    class Program
    {
        static void Main()
        {
            //999
            //99-999
            //999-999
            while (true)
            {
                Console.Write("Adj meg egy MCP vizsgakódot: ");
                string input = Console.ReadLine();

                string regex1 = @"^(\d{2,3}-)?\d{3}$";
                //string regex2 = @"^\d{3}|(\d{2}-\d{3})|(\d{3}-\d{3})$";

                if (Regex.IsMatch(input, regex1))
                {
                    Console.WriteLine("Ez megfelelő formátumú.");

                    string regex4 = "[a-zA-ZÁÉÍÓŐÜŰ]";

                    string regex3 = @"^(\d{2,3}-)?(?<lastnumbers>\d{3})$";
                    Regex rx = new Regex(regex3);
                    Match m = rx.Match(input);
                    //Console.WriteLine(m.Groups["lastnumbers"]);
                    //Console.WriteLine(m.Groups[1]);
                    Console.WriteLine(m.Result("${lastnumbers}"));
                }
                else
                {
                    Console.WriteLine("Nem megfelelő formátum.");
                }
            }


            Console.ReadLine();
        }
    }
}